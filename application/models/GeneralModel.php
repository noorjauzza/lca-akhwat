<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Region model class
 *
 * @author Cecep Sutisna
 */
class GeneralModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrieve complete data from database
     *
     * @return array
     */

    public function checkDataUser($username, $password){
        $level = '(level_user = 16 or level_user = 17)';
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password_asli', $password);
        $this->db->where($level);
        return $this->db->get('user_akses')->row();
    }

    public function checkDataById($table, $id)
    {
        $this->db->where('id', $id);
        return $this->db->get($table)->row();
    }

    public function create($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function insert($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update($table, $id, $data)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            return $this->db->update($table, $data);
        } else {
            return false;
        }
    }

    public function delete($table, $id)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            return $this->db->delete($table);
        } else {
            return false;
        }
    }

    // Model Keuangan
    public function getKeuangan($table, $level, $tipe)
    {
        $this->db->select('*');
        $this->db->where('level', $level);
        if(!empty($tipe)){
            $this->db->where('tipe', $tipe);
        }
        $this->db->order_by('dibuat', 'desc');
        return $this->db->get($table)->result();
    }

    public function getKeuanganJumlah($table, $level, $tipe)
    {
        $this->db->select_sum('nominal');
        $this->db->where('level', $level);
        if(!empty($tipe)){
            $this->db->where('tipe', $tipe);
        }
        return $this->db->get($table)->row();
    }
}