<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Welcome model class
 *
 * @author Cecep Sutisna
 */
class WelcomeModel extends CI_model
{
    public function retrieveSchoolSettings()
    {
        $this->db->where('id', 1);
        $data = $this->db->get('school')->last_row();

        return $data;
    }

    public function getMenu($role)
    {
        $returnval = array();
        $this->db->select('*');
        $this->db->order_by("sort_number", "asc");
        $menu = $this->db->get('ui_menu')->result();

        foreach ($menu as $rowmenu) {
            $allowed_user = explode(',', $rowmenu->role);

            if (in_array($role, $allowed_user) && $rowmenu->enabled == 1) {
                if ($rowmenu->url == 'index') {
                    $this->db->where('menu_id', $rowmenu->id);
                    $submenu = $this->db->get('ui_submenu')->result();

                    foreach ($submenu as $rowsubmenu) {
                        $rowmenu->submenu[] = $rowsubmenu;
                    }
                }

                $returnval[] = $rowmenu;
            }
        }

        return $returnval;
    }
}
