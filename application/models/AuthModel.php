<?php
defined('BASEPATH') or exit('No direct script access allowed');
class AuthModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function checkUser($username)
    {
        $this->db->select('*', false);
        $this->db->where('username', $username);
        return $this->db->get('users')->row();
    }

    public function insertData($data)
    {
        return $this->db->insert('users', $data);
    }

    public function updateData($id, $data)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            return $this->db->update('users', $data);
        } else {
            return false;
        }
    }

    public function deleteData($id)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
            return $this->db->delete('users');
        } else {
            return false;
        }
    }

    public function updatePass($username, $tempPass)
    {
        $password       = password_hash($tempPass, PASSWORD_BCRYPT, array('cost' => 11));
        $this->db->where('username', $username);
        return $this->db->update('users', array('password' => $password));
    }
}
