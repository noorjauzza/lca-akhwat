<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("GeneralModel", "model");
        $this->twig->addGlobal('menu', 'beranda');
        $this->twig->addGlobal('title', 'Beranda');
        
    }

    public function index($username = 0, $password = 0)
    {
        $data['path']       = 'beranda';
        $getUser            = $this->model->checkDataUser($username, $password);
        if(isset($getUser)){
            $this->session->set_userdata((array) $getUser);
            $data['data']       = $getUser;
            redirect('Keuangan');
        }else{
            redirect('https://operasional.gencarlca.id/public/auth');
        }
    }
}
