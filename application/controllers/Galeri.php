<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Galeri extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("GeneralModel", "model");
        $this->twig->addGlobal('menu', 'galeri');
        $this->twig->addGlobal('title', 'Galeri');
        
    }

    public function index()
    {
        $data['path']       = 'beranda';
        $this->twig->display('components/default/default', $data);
    }
}
