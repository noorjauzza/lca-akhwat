<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keuangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("GeneralModel", "model");
        $this->twig->addGlobal('menu', 'keuangan');
        $this->twig->addGlobal('title', 'Keuangan');
        if (!$this->session->userdata('id')) {
            redirect('https://operasional.gencarlca.id/public/auth');
        }elseif($this->session->userdata('level_user') != '16' && $this->session->userdata('level_user') != '17'){
            redirect('Notfound');
        }
    }

    public function index()
    {
        $data['path']       = 'keuangan';
        $this->twig->display('components/keuangan/main', $data);
    }

    public function pemasukan()
    {
        $role	= $this->session->userdata('level_user');
        $data['data'] = $this->model->getKeuangan('akhwat_keuangan', $role, 1);
        $data['counting'] = $this->model->getKeuanganJumlah('akhwat_keuangan', $role, 1);
        $this->twig->display('components/keuangan/pemasukan', $data);
    }

    public function pengeluaran()
    {
        $role	= $this->session->userdata('level_user');
        $data['data'] = $this->model->getKeuangan('akhwat_keuangan', $role, 2);
        $data['counting'] = $this->model->getKeuanganJumlah('akhwat_keuangan', $role, 2);
        $this->twig->display('components/keuangan/pengeluaran', $data);
    }

    public function laporan()
    {
        $role	= $this->session->userdata('level_user');
        $data['data'] = $this->model->getKeuangan('akhwat_keuangan', $role, null);
        $pemasukan = $this->model->getKeuanganJumlah('akhwat_keuangan', $role, 1);
        $pengeluaran = $this->model->getKeuanganJumlah('akhwat_keuangan', $role, 2);
		$data['counting'] = @($pemasukan->nominal - $pengeluaran->nominal);
        $this->twig->display('components/keuangan/laporan', $data);
    }

    public function simpan_keuangan()
    {
        $post            = $this->input->post(null, true);

		$response        = array();
		$nominal    	 = $post['nominal'];
		$total_str 		 = preg_replace("/[^0-9]/", "", $nominal);
		$total_int 		 = (int) $total_str;
		$tanggal    	 = $post['tanggal'];
		$jam    	     = $post['jam'];
		$tipe    	     = $post['tipe'];
		$keterangan      = $post['keterangan'];
		$date			 = date('Y-m-d', strtotime($tanggal));

		$param                  = array();
		$param['pj']  			= $this->session->userdata('nama');
		$param['level']  		= $this->session->userdata('level_user');
		$param['dibuat']        = $date . ' ' . $jam . ':00';
		$param['nominal']  		= $total_int;
		$param['tipe']  		= $tipe;
		$param['keterangan']  	= $keterangan;


		$prosesData            = $this->model->create('akhwat_keuangan', $param);

		if ($prosesData) {
			$response['status']     = true;
			$response['message']    = 'Data berhasil ditambahkan !';
		} else {
			$response['status']     = false;
			$response['message']    = 'Data gagal ditambahkan !';
		}
		die(json_encode($response));
    }
}
