<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("GeneralModel", "model");
        $this->twig->addGlobal('menu', 'beranda');
        $this->twig->addGlobal('title', 'Beranda');
        
    }

    public function index()
    {
        $data['path']       = 'beranda';
        $this->twig->display('components/default/default', $data);
    }
}
