<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anggota extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("GeneralModel", "model");
        $this->twig->addGlobal('menu', 'anggota');
    }

    public function index()
    {
        $data['path']       = 'anggota';
        $this->twig->addGlobal('title', 'Data Alumni Miftahul Jannah');
        // $this->twig->display('components/alumni/alumni');
        $this->twig->display('components/default/default');
    }

    public function alumni($id)
    {
        $data['data']    = $this->model->retrieveSelect('alumni', $id);
        $this->twig->addGlobal('title', 'Anggota Angkatan ' . $id . ' [ ' . count($data['data']) . ' ]');
        $data['tingkat'] = $id;
        $this->twig->display('components/alumni/detail', $data);
    }

    public function simpan_data()
    {
        
        $post            = $this->input->post(null, true);

        $response        = array();
        $id              = $post['id'];
        $nama    		 = $post['nama'];
        $santri    		 = $post['santri'];
        $phone           = $post['phone'];
        $alamat    	     = $post['alamat'];
        $tingkat    	 = $post['angkatan'];
        $foto            = $_FILES['foto']['name'];
        $config['upload_path'] 		= 'assets/images/alumni';
        $config['allowed_types'] 	= 'jpeg|jpg|png';
        $config['max_size']     	= '3000';
        $config['rewrite']			= true;

        $this->upload->initialize($config);
        if ($this->upload->do_upload('foto')) {
            $foto             = $this->upload->data('file_name');
        }

        $param                  = array();
        $param['foto']          = base_url() . 'assets/images/alumni/' . $foto;
        $param['name']  		= $nama;
        $param['phone']     	= $phone;
        $param['gender']    	= $santri;
        $param['address']    	= $alamat;
        $param['angkatan']      = $tingkat;
        $param['created_at']    = date('Y-m-d H:i:s');
        $param['updated_at']    = date('Y-m-d H:i:s');
        
            if (empty($id)) {
                $check = $this->model->checkName('alumni', $nama);
                if(isset($check)){
                    $response['status']     = false;
                    $response['link']       = $tingkat;
                    $response['message']    = 'Nama Sudah Terdaftar !';
                }else{
                    $prosesData            = $this->model->create('alumni', $param);

                    if ($prosesData) {
                        $response['status']     = true;
                        $response['link']       = $tingkat;
                        $response['message']    = 'Data berhasil ditambahkan !';
                    } else {
                        $response['status']     = false;
                        $response['link']       = $tingkat;
                        $response['message']    = 'Data gagal ditambahkan !';
                    }
                }
            } else {
                $this->model->update('alumni', $id, $param);

                $prosesData     = $id;
                if ($prosesData) {
                    $response['status']     = true;
                    $response['link']       = $tingkat;
                    $response['message']    = 'Data berhasil diperbaharui !';
                } else {
                    $response['status']     = false;
                    $response['link']       = $tingkat;
                    $response['message']    = 'Data gagal diperbaharui !';
                }
            }
        die(json_encode($response));
    }
}
